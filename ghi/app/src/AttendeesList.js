function AttendeesList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
        {this.state.states.map(state => {
          return (
            <option key={state.abbreviation} value={state.abbreviation}>
              {state.name}
            </option>
          );
        })}
        </tbody>
      </table>
    );
  }
  
  export default AttendeesList;

  