import Nav from './Nav';



function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <p class="card-footer">${startDate} to ${endDate}</p>
        </div>
      </div>
    `;
  }
  
  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        console.error("ERROR! DANGER WILL ROBINSON!");

        const newHTML = alertComponent();
        const somethingWrong = document.querySelector("#something-wrong");
        somethingWrong.innerHTML = newHTML;
      } else {
        const data = await response.json();
        let i = 1
        
        for (let conference of data.conferences) {
            if (i > 3) {
                i = 1
            }
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const location = details.conference.location.name;
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate1 = details.conference.starts;
            const startDate = startDate1.slice(0,10);
            const endDate1 = details.conference.ends;
            const endDate = endDate1.slice(0,10);
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector(`#col${i}`);
            column.innerHTML += html;
            i++
          }
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      function alertComponent() {
        return `
        <div id="something-wrong" class="alert alert-primary" role="alert">
        Something bad happened
        </div>
        `
      }
        var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
        var alertTrigger = document.getElementById('liveAlertBtn')

        function alert(message, type) {
        var wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

        alertPlaceholder.append(wrapper)
        }

        if (alertTrigger) {
        alertTrigger.addEventListener('click', function () {
            alert('Nice, you triggered this alert message!', 'success')
        })
        }
    }
  
  });
